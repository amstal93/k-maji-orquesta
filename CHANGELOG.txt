# Change Log

## [v1.4.0](https://www.kronops.com.mx) (2021-04-30)

**features**

- Update bootstrap and deploy scripts for ubuntu focal.
- Update metadata for ubuntu focal for general system and jenkins roles.
- Update jenkins role tasks for ubuntu fucal.
- Update kvm-guests role to support qemu-guest-agent package.
- Update package-mgmt role to support ubuntu focal.
- Update server-images to change support from xenial to focal.
- Update server-images readme to support ubuntu focal images.
- Remove server-images build for oraclelinux6.
- Update vagrant files for ubuntu focal.
- Update Readme with ubuntu 2004 instructions.

**fixes**

- Fix awscli and terraform plabyook syntax errors.

## [v1.3.1](https://www.kronops.com.mx) (2019-10-07)

**fixes**

* Changes group names formatting due deprecated style
* Ansibles tests does work again

**features**

* New playbook and Jenkins job for DNS service
* New Documentation now comes in jekill's static site application (Runs in docker)

## [v1.3.0](https://www.kronops.com.mx) (2019-08-18)

**fixes**

* Add roles path to ansible.cfg
* Fix virt-host playbook to new standard
* Jenkins jobs fix permissions for deployer y tester
* Inspec licenses correction
* Selenium fixes for chrome version
* Initial deploy and bootstrap parametrization

**features**

* Packer server images integration
* New python3 integration
* New playbook for bacula-server
* New playbook for bacula-client
* New playbook template
* New playbook for docker
* New playbook for terraform
* New playbook for kubectl
* New playbook for helm
* New pipeline for new cloud tools

## [v1.2.3](https://www.kronops.com.mx) (2019-06-13)

**fixes**

* Change how to reference git repository, implement REPO_URL.
* Change how to reference local deploy path, implement REPO_ROOT.

**features**

* Integrate proxy support for packer build images.
* Standarize kickstart file for centos7, set default language to en_US UTF8.
* Standarize kickstart file for oraclelinux7, set default language to en_US UTF8.
* Standarize kickstart file for oraclelinux6, set default language to en_US UTF8.
* Create ansible role to set packer on kvm builder, creat iso path and images.
* Integrate testinfra for kvm-templates and packer-service.
* Update readme for server-images, new explanations.
* Actualizar diagrama de arquitectura con el componente server-images.
* Integrate terraform on deployer, ansible role, testinfra class and jenkins jobs and pipeline.

## [v1.2.2](https://www.kronops.com.mx) (2019-04-25)
* Inspec gives more detailed output from patching and hardening audit
* Results analyzer output is sorted by nodes and certification rules.

## [v1.2.1](https://www.kronops.com.mx) (2019-04-05)

* Updated documentation, now includes Pipelines builds description, added proxy settings, rearranged some topic's relevance.

## [v1.2.0](https://www.kronops.com.mx) (2019-02-25)

* Update iTop 2.6.0 version.
* Added env_prefix to vms.
* Added proxy setting to Ansible vars file.
* Added proxy setting to jenkins groovy script.
* Added hardening_vars under ansible vars for stig profile.
* Added inspec-stig-rhel7 profile for inSpec.
* Added patching and hardening roles for hardening pipeline.
* Added dynamic_inventory script for ansible and iTop.
* Change jenkins plugins installation method in Ansible.
* Change junit xml file for inSpec hardening tests.
* Clean aplication playbooks syntax.

## [v1.1.8](https://www.kronops.com.mx) (2019-02-25)

** features **

* Se integra nuevo rol de ansible para hacer hardening basado en las rhel7-stig.
* Se integra nueva herramienta de pruebas Inspec para hacer compliance as code.
* Se integra nuevo rol de ansible para hacer inspección de hardening de las rhel7-stig.

**fixes**

* Muchas correcciones en el despliegue inicial, tanto en documentación, makefile, shell scripts, playbooks, jobs y pipelines de jenkins.
* Mejoras soporte proxy HTTP en todos los componentes, como vagrant, shell, ansible y jenkins.
* Mejoras en el despliegue de iTop con datos de ejemplo, carga automática de solucione aplicativas.
* Mejoras en la integración de Ansible2iTop lo cual integra cmdb de itop como inventario dínamico.
* Integración servicio monitor por default con nagios core.
* Integracion de construcción de plantillas de OS para OracleLinux7 y OracleLinux6.

## [v1.1.5](https://www.kronops.com.mx) (2019-01-11)

** features **
* Mejoras en proceso para deployar k-maji-orquesta.
* Se agregan pasos para configurar el proxy.
* Actualización de READMES de depliegue.
* Parametrización de BRANCH en Jenkins.
* Nuevo script para deployar ansible nuevos hosts.

**fixes**
* Reconstrucción del pipeline deploy-orquesta.
* Renombre y refactor de scripts de instalación de k-maji-orquesta.
* Renombre de componentes monitor y cmdb.
* Fix en tags y testinfra en deploy de postgres.
* Se cambia la instalación de chromedriver en tester.
* Se limpian variables de proxy para el uso desactivado.

** deletes **
* Se quita del README de setup-vagrant.
* Se quita el script postSetup.
* Se quita el binario chromedriver en el rol de tester.

## [v1.1.4](https://www.kronops.com.mx) (2018-12-20)

**fixes**

* Mejora

* Se Cambia el nombre del repo a k-maji-orquesta
* Se modifica el deploy de artefactos del pipeline orquesta.
* Se actualizan los roles de ansible para el estandar de instalación de apt.
* Se refactoriza el rol de jenkins-service de ansible.
* Se quitan plugins de jenkins deprecados.
* Se homologan nombres en pipelines y jobs de Jenkins.
* Mejora en la documentación inicial y de testinfra.

** features **

* Se agrega el plugin env y proxy a Vagrant
* Se agrega compatibilidad con proxy a ansible y vagrant 

## [v1.1.3](https://www.kronops.com.mx) (2018-11-29)

**fixes**

* Mejora en la organización raíz de Vagrant.
* Mejoras en la homologación de títulos en Pipelines.
* Mejoras en tester/testinfra para la creación más simple de tests.
* Se refactoriza y renombra el rol common por general-settings de ansible.
* Se refactorizan los principales roles de ansible en base a las mejores prácticas.

** deletes **

* Limpieza de tags en roles y jenkins para un mejor mantenimiento de los mimos.
* Se deja únicamente el tag: app_deploy para deployar dentro de Jenkins.

** features **

* Cambio de nombre de la máquina devops a deployer.
* Nuevo rol de nagios para ansible con monitoreo integrado de las máquinas desplegadas.
* Se agregan los eventhandlers de nagios que automatizan la creación de tickets en iTop.
* Actualización de iTop a la versión 2.5.0
* Se agrega un script en python para crear objetos en iTop usando su API y se cargan objetos deployados.



## [v1.1.2](https://www.kronops.com.mx) (2018-10-17)

**fixes**

* Mejor ortografía y gramática en la documentación del setup.
* Mejores validaciones en los scripts de setup inicial.
* Mejoras en la organización de la estructura raíz del proyecto.
* Limpiezas generales en scripts Makefile y build.sh.
* Correcciones en postsetup para copiar .env e inventario a home jenkins.
* Correcciones en pipeline de despliegue de orquestador, ya respeta el uso del .env.
* Correcciones para despliegue de jobs de tester en pipeline principal.

**deletes**

* Se quita soporte proxy http en environments de playbooks ansible.

**features**

* Nuevo rol de ansible para el despliegue de servidor postgresql.
* Nuevo testcase de testinfra para el servidor postgresql.
* Nueva maquina postgres al inventario creado por setup.
* Nuevos a Jenkins jobs y pipeline de despliegue y pruebas de postgresql.
* Personalizamos el setup para instalar las otras vms de itop, wordpress, postgresql.
* Simplificación de la documentación general y de setup inicial.
* Mejoras configuración de servidor postgresql para crear bases de datos, usuarios y tareas generales.
* Mejoras en el setup del tester, se desacopla del setup inicial, tiene pipeline propio.

## [v1.1.1](https://www.kronops.com.mx) (2018-09-28)

En esta versión incluimos diferentes cambios con colaboración de Axel y Luis, lo más relevante es lo siguiente:

**features**

* Mejoras en la documentación de el setup inicial.
* Mejoras en los scripts de setup inicial tanto para vagrant y en cloud.
* Mejoras en la integración de testinfra a los pipelines de Jenkins, Wordpress y iTop.
* Mejoras en la visiblidad de los pipelines en jenkins integrando el plugin oceanblue.
* Cambio de nombre de grupo o equipo, de koolops a kronops, se cambian referencias internas.

## [v1.1.0](https://www.kronops.com.mx) (2018-09-16)

Estos son los cambios más relevantes:

* Agregamos documentación para uso de gitlab-flow en docs/README-setup.md.
* Separación de los componentes de testing de la máquina devops a la maquina tester.
* Agregamos soporte para swarm como agente de slaves jenkins a través del rol swarm-service.
* Agregamos integración de jobs de jenkins para testing en equípo tester slave con swarm.
* Introducimos el uso de packer para generar imagenes de qemu-kvm, ver directorio server-images.
* Mejoramos la documentación para instalar el sistema de orquetación en vagrant.
* Mejoramos el código de testing de selenium y testinfra.
* Mejoramos los scripts para generar y distribuír llaves SSH a todos los servidores.
* Introducimos iTop como aplicación para procesos operativos de TI con CMDB.

## [v1.0.9](https://www.kronops.com.mx) (2018-07-09)

## [v1.0.8](https://www.kronops.com.mx) (2018-07-05)

## [v1.0.7](https://www.kronops.com.mx) (2018-06-18)

## [v1.0.6](https://www.kronops.com.mx) (2018-06-04)

## [v1.0.5](https://www.kronops.com.mx) (2018-05-07)

## [v1.0.4](https://www.kronops.com.mx) (2018-03-18)

## [v1.0.3](https://www.kronops.com.mx) (2017-11-20)

## [v1.0.2](https://www.kronops.com.mx) (2017-09-16)

## [v1.0.1](https://www.kronops.com.mx) (2017-07-27)

## [v1.0](https://www.kronops.com.mx) (2017-07-24)

## OLD Change Log for server-images

### [1.0.4](https://www.kronops.com.mx) (2018-05-12)

**Improvements**
- Fixes on README

### [1.0.3](https://www.kronops.com.mx) (2018-05-12)

**Improvements**
- Homologacion construiccion packer para ubuntu 16.04
- Se agrega documentacion para comunidad.

### [1.0.2](https://www.kronops.com.mx) (2018-05-12)

**Improvements**
- Limpieza build files
- Se quita compatilibdad rhel6 y derivados
- Se quita compatilibdad ubuntu14.04 y derivados

### [0.2.0](https://www.kronops.com.mx) (2017-09-03)

**New**
- CentOS 7.3 64 bit. (vagrant/qemu)

**Improvements**
- Build new version 0.2.0
- New ssh-root-key.sh.
- More homologation between Ubuntu and OracleLinux images.

### [0.1.0](https://www.kronops.com.mx) (2017-09-02)

**New**
- Ubuntu 14.04 64 bit.
- Ubuntu 16.04 64 bit.
- OracleLinux 7.3 64 bit.

**Improvements**
- Configuration template homologation.
- Provisioning shell scripts separated for os.
- New box stage on Makefile for vagrant box.
