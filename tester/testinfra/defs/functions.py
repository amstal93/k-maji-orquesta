# -*- coding: UTF-8 -*-

'''
Tests infra custom functions
'''

import sys
import os

import testinfra
from termcolor import colored
from terminaltables import AsciiTable as Table

def checkFileContains(host, str, file):
    myFile = host.file(file)
    contains = myFile.contains(str)
    return contains

def test_file_contains(host, contains, out):
    out.append(['\n[+] %s' % yellow('Check file contains'), ''])
    for dict in contains:
        if os.path.isfile(dict['file']):
            response = checkFileContains(host, dict['str'], dict['file'])
            out.append([
               ' - %s, in %s' % (dict['str'], dict['file']),
               "%s" % (green('[ True ]') if response else red('[ False ]'))
            ])
            if not response: assert False
        else:
            out.append([
              ' - %s Not exist!!' % dict['file'],
              white('[ WARNING ]')
            ])
            assert False

def test_not_in_file(host, not_contains, out):
    out.append(['\n[+] %s' % yellow('Check not in file'), ''])
    for dict in not_contains:
        if os.path.isfile(dict['file']):
            response = checkFileContains(host, dict['str'], dict['file'])
            if not response:
                out.append([
                   ' - %s, in %s' % (dict['str'], dict['file']),
                   "%s" % (green('[ True ]') if response else red('[ False ]'))
                ])
            else: assert False
        else:
            out.append([
              ' - %s Not exist!!' % dict['file'],
              white('[ WARNING ]')
            ])
            assert False

def checkServiceRunning(host, serviceName):
    service = host.service(serviceName)
    running = service.is_running
    return running

def test_service_running(host, services, out):
    """ Check services running """
    out.append(['\n[+] %s' % yellow('Check services running'), ''])
    for service in services:
        response = checkServiceRunning(host, service)
        out.append([
          ' - %s running' % service,
          "%s" % (green('[ True ]') if response else red('[ False ]'))
        ])
        if not response: assert False

def checkServiceEnabled(host, serviceName):
    service = host.service(serviceName)
    enabled = service.is_enabled
    return enabled

def test_service_enabled(host, services, out):
    """ Check services enabled """
    out.append(['\n[+] %s' % yellow('Check services enabled'), ''])
    for service in services:
        response = checkServiceEnabled(host, service)
        out.append([
          ' - %s enabled' % service,
          "%s" % (green('[ True ]') if response else red('[ False ]'))
        ])
        if not response: assert False

def checkFileExist(host, fileName):
    hostFile = host.file(fileName)
    return hostFile.is_file

def test_file_exist(host, files, out):
    """ Check file exist """
    out.append(['\n[+] %s' % yellow('Check file exist'), ''])
    for file in files:
        response = checkFileExist(host, file)
        out.append([
          ' - %s exist' % file,
          "%s" % (green('[ True ]') if response else red('[ False ]'))
        ])
        if not response: assert False

def isDirectory(host, myDir):
    hostFile = host.file(myDir)
    return hostFile.is_directory

def test_dir_exist(host, dirs, out):
    """ Check Dir exist """
    out.append(['\n[+] %s' % yellow('Check directory exist'), ''])
    for dir in dirs:
        response = isDirectory(host, dir)
        out.append([
          ' - %s exist' % dir,
          "%s" % (green('[ True ]') if response else red('[ False ]'))
        ])
        if not response: assert False

def checkFileUser(host, user, fileName):
    hostFile = host.file(fileName)
    isUser = hostFile.user == user
    return isUser

def checkFileGroup(host, group, fileName):
    hostFile = host.file(fileName)
    isGroup = hostFile.group == group
    return isGroup

def checkFileMode(host, mode, fileName):
    hostFile = oct(host.file(fileName).mode)
    isMode = hostFile == mode
    return isMode

def test_file_mode(host, object, out):
    """ Check Files mode """
    out.append(['\n[+] %s' % yellow('Check File mode'), ''])
    for dict in object:
        if os.path.isfile(dict['file']):
            response = checkFileMode(host, dict['mode'], dict['file'])
            out.append([
               ' - %s mode %s' % (dict['file'], dict['mode']),
               "%s" % (green('[ True ]') if response else red('[ False ]'))
            ])
            if not response: assert False
        else:
            out.append([
              ' - %s Not exist!!' % dict['file'],
              white('[ WARNING ]')
            ])
            assert False

def checkPkg(host, package):
    hostPackage = host.package(package)
    return hostPackage.is_installed

def test_pkgs(host, packages, out):
    """ Check packages installed """
    out.append(['[+] %s' % yellow('Check packages installed'), ''])
    for pkg in packages:
        response = checkPkg(host, pkg)
        out.append([
          ' -  %s installed' % pkg,
          "%s" % (green('[ True ]') if response else red('[ False ]'))
        ])
        if not response: assert False

def checkPort(host, port):
    port, protocol = port.split('/')
    hostSocket = host.socket('%s://%s' % (protocol, port))
    return hostSocket.is_listening

def test_ports(host, ports=[], out=[]):
    """ Check ports listening """
    out.append(['\n[+] %s' % yellow('Check ports listening'), ''])
    for port in ports:
        response = checkPort(host, port)
        out.append([
          ' - %s listening' % port,
          "%s" % (green('[ True ]') if response else red('[ False ]'))
        ])
        if not response: assert False


def resultTable(outputList):
    if len(outputList) > 1:
        out = Table(outputList)
        #out.justify_rows[0] = 'center'
        out.inner_row_border = False
        for i in range(len(outputList[0])): out.justify_columns[i] = 'left'
        print("\n%s\n" % out.table)

def initTable():
    """ Initialization of the result table output """
    output = []
    output.append(['[+] %s' % blue('Test'), blue('Out')])
    return output

def blue(var):
    return colored(var, 'blue', attrs=['bold'])

def green(var):
    return colored(var, 'green', attrs=['bold'])

def grey(var):
    return colored(var, 'grey', attrs=['bold'])

def red(var):
    return colored(var, 'red', attrs=['bold'])

def white(var):
    return colored(var, 'white', attrs=['bold'])

def yellow(var):
    return colored(var, 'yellow', attrs=['bold'])

def writeFile(content, myFile):
    file = open(myFile, 'w+')
    file.write(content)
    file.close()
    if not os.path.isfile(myFile):
        return None

def readFile(myFile):
    if os.path.isfile(myFile):
        print (myFile)
        file = open(myFile,'r')
        return file.read()
    else:
        return None

# def find_in_file(str, file):
#     flag = False
#     with open('myfile.txt') as myfile:
#         if str in myfile.read():
#             flag = True
#     return flag

def cleanXML():
    from subprocess import call
    files = os.popen("ls -lrth ./xmlresult | tail -n 1 | awk '{print $9}'").readlines()
    call("sed -i 's/\x1B\[[0-9;]*[JKmsu]//g' ./xmlresult/%s" % files[0], shell=True)
    return files
