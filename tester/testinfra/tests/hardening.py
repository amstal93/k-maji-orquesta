#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Hardening(TestCase):
    def test_file_contains(s):
        contains = [
          {'file':'/etc/ssh/sshd_config','str':'PermitRootLogin yes'},
        ]
        f.test_file_contains(s.host, contains, s.output)

    def test_not_in_file(s):
        not_contains = [
          {'file':'/etc/ssh/sshd_config','str':'PermitRootLogin no'},
        ]
        f.test_not_in_file(s.host, not_contains, s.output)

    def test_file_exist(s):
        files = [
            '/etc/aide/aide.conf.d',
            '/etc/aide.conf',
            '/etc/cron.allow',
        ]
        f.test_file_exist(s.host, files, s.output)

    def test_service_running(s):
        services = ['apparmor', 'autofs', 'clamav', 'firewalld']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)

    def test_file_mode(s):
        fileMode = [
          {'file':'/etc/ssh/ssh_host_dsa_key.pub','mode':'0644'},
          {'file':'/etc/ssh/ssh_host_ecdsa_key.pub','mode':'0644'},
          {'file':'/etc/ssh/ssh_host_ed25519_key.pub','mode':'0644'},
          {'file':'/etc/ssh/ssh_host_rsa_key.pub','mode':'0644'},
          {'file':'/etc/ssh/ssh_host_dsa_key','mode':'0600'},
          {'file':'/etc/ssh/ssh_host_ecdsa_key','mode':'0600'},
          {'file':'/etc/ssh/ssh_host_ed25519_key','mode':'0600'},
          {'file':'/etc/ssh/ssh_host_rsa_key','mode':'0600'},
        ]
        f.test_file_mode(s.host, fileMode, s.output)
